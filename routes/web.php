<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/', 'PostController@dashbord');
Route::get('/create', 'PostController@postCreate')->name('/create');
Route::post('/createPost', 'PostController@create')->name('/createPost');
Route::get('/delete/{id?}', 'PostController@delete')->name('/delete');
Route::get('/edit/{id?}', 'PostController@edtitView')->name('/edit');
Route::post('/editPost/{id?}', 'PostController@editPost')->name('/editPost');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
