@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4"><a href="{{route('/create')}}">Create Article</a></div>
        </div>
        <div class="row">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <h2>Articles</h2>
                </div>
                <div class="row">
                    @foreach($posts as $post)
                        <div class="col-md-6">
                            Title: {{$post->title}}
                            <img src="{{$post->img}}" style="width: 200px; height: 150px">
                            Description: {{$post->description}}
                        </div>
                        @csrf
                    <div class="col-md-1">
                        <a href="{{route('/edit',['id' => $post->id])}}">EDIT</a>
                    </div>
                        <div class="col-md-1">
                        <a href="{{route('/delete', ['id' => $post->id])}}">DELETE</a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection