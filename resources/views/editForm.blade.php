@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <h2>Create post</h2>
                </div>
                <div class="row">
                    <form action="{{route('/createPost', ['id' => $post->id])}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <h3>Title:</h3>
                            </div>
                            <div class="col-md-6">
                                <input type="text" name="title" value="{{$post->title}}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <h3>Description:</h3>
                            </div>
                            <div class="col-md-6">
                                <input type="text" name="description" value="{{$post->description}}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <h3>image:</h3>
                            </div>
                            <div class="col-md-6">
                                <input type="text" name="image" value="{{$post->image}}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="submit" value="SUBMIT">
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection