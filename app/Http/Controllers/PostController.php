<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function dashbord()
    {
        $posts = Post::all();

        return view('dashbord', ['posts' => $posts]);
    }

    public function postCreate()
    {
        return view('createPost');
    }

    public function create(Request $request)
    {
        $post = new Post();
        $post->title = $request['title'];
        $post->description = $request['description'];
        $post->image = $request['image'];
        $post->view = 1;
        $post->save();

        return redirect()->back();
    }

    public function delete($id)
    {
        $post = Post::where('id', '=', $id)->delete();

        return redirect()->back();
    }

    public function edtitView($id)
    {
        $post = Post::where('id', '=', $id)->first();
        return view('editForm', ['post' => $post]);
    }

    public function editPost(Request $request, $id)
    {
        Post::where('id', $id)->update([
            'title' => $request['title'],
            'description' => $request['description'],
            'image' => $request['image']
        ]);


        return redirect()->back();
    }
}
